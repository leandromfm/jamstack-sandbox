import Head from 'next/head';
import styles from './styles.module.scss';

export default function Articles() {
  return (
    <>
      <Head>
        <title>Articles | Journal</title>
      </Head>
      <main className={styles.container}>
        <div className={styles.articles}>
          <a href=''>
            <time>August 10, 2021</time>
            <strong>Lorem Ipsum</strong>
            <p>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam
              corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
              consequatur?
            </p>
          </a>
          <a href=''>
            <time>August 10, 2021</time>
            <strong>Lorem Ipsum</strong>
            <p>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam
              corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
              consequatur?
            </p>
          </a>
          <a href=''>
            <time>August 10, 2021</time>
            <strong>Lorem Ipsum</strong>
            <p>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam
              corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
              consequatur?
            </p>
          </a>
        </div>
      </main>
    </>
  );
}
