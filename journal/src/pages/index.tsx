import { GetServerSideProps, GetStaticProps } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { Props } from 'react';

import AvatarSvg from '../../public/images/avatar.svg';
import { SubscribeButton } from '../components/SubscribeButton';

import { stripe } from '../services/stripe';

import styles from './home.module.scss';

// Client-side
// Server-side
// Static site

// post of this journal could be static site generation with

// however, the comment section, does not need to be used before the page be downloaded. Then a client-side request is better suited.

interface HomeProps {
  product: {
    priceId: string;
    amount: number;
  };
}

export default function Home({ product }: HomeProps) {
  return (
    <>
      <Head>
        <title>Home | Journal</title>
      </Head>
      <main className={styles.contentContainer}>
        <Image src={AvatarSvg} alt='Team working in software development' />
        <section className={styles.hero}>
          <span>👋 Hey, welcome!!!</span>
          <h1>
            Articles about <span>Computer Science</span>
          </h1>
          <p>
            Get access to all publication{' '}
            <span>for only {product.amount} per month</span>
          </p>
          <SubscribeButton priceId={product.priceId} />
        </section>
      </main>
    </>
  );
}

// export const getServerSideProps: GetServerSideProps = async () => {
//   const price = await stripe.prices.retrieve('price_1JgJ25FU7titZ81943ZpF601', {
//     expand: ['product'],
//   });

//   const product = {
//     priceId: price.id,
//     amount: new Intl.NumberFormat('en-US', {
//       style: 'currency',
//       currency: 'CAD',
//     }).format((price.unit_amount ? price.unit_amount : 0) / 100),
//   };

//   return {
//     props: {
//       product,
//     },
//   };
// };

export const getStaticProps: GetStaticProps = async () => {
  const price = await stripe.prices.retrieve('price_1JgJ25FU7titZ81943ZpF601', {
    expand: ['product'],
  });

  const product = {
    priceId: price.id,
    amount: new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'CAD',
    }).format((price.unit_amount ? price.unit_amount : 0) / 100),
  };

  return {
    props: {
      product,
    },
    revalidate: 60 * 60 * 24, // (sec) * (min) * (hours) 24hours
  };
};
