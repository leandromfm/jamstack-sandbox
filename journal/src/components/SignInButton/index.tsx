import { signIn, signOut, useSession } from 'next-auth/client';

import { FaGitlab } from 'react-icons/fa';
import { FiX } from 'react-icons/fi';

import styles from './styles.module.scss';

export function SignInButton() {
  const [session] = useSession();

  // console.log(session);

  return session ? (
    <button
      type='button'
      className={styles.signInButton}
      onClick={() => signOut()}
    >
      <FaGitlab color='#04d361' />
      Welcome {session.user?.name}
      <FiX color='#737380' className={styles.closeIcon} />
    </button>
  ) : (
    <button
      type='button'
      className={styles.signInButton}
      onClick={() => signIn('gitlab')}
    >
      <FaGitlab color='#eba417' />
      Sign In with GitLab
      <FiX color='#737380' className={styles.closeIcon} />
    </button>
  );
}

// HTTP, REST ...
// connection do not to be persisted.
// MongoDB, PostgreSQL ... 24h - 1 connection / even if "serveless" #authentication is limited.
