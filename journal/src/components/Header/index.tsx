import Image from 'next/image';

import { SignInButton } from '../SignInButton';

import LogoImg from '../../../public/images/logo.svg';
import styles from './styles.module.scss';

export function Header() {
  return (
    <header className={styles.headerContainer}>
      <div className={styles.headerContent}>
        <Image src={LogoImg} alt='React logo image' />
        <nav>
          <a className={styles.active}>Home</a>
          <a href='/articles'>Articles</a>
        </nav>
        <SignInButton />
      </div>
    </header>
  );
}
