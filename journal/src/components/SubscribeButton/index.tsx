import { signIn, useSession } from 'next-auth/client';
import { api } from '../../services/api';
import { getStripeJs } from '../../services/stripe-js';
import styles from './styles.module.scss';

// Keep secuity always in mind!!!
// getServerSideProps ...
// getSaticProps ...
// API routes...

interface SubscribeButtonProps {
  priceId: string;
}

export function SubscribeButton({ priceId }: SubscribeButtonProps) {
  const [session] = useSession();

  const handleSubcribe = async () => {
    if (!session) {
      signIn('gitlab');
      return;
    }

    // create the checkout session:
    // try {
    const response = await api.post('subscribe');
    const { sessionId } = response.data;

    // const stripe = await getStripeJs();
    // stripe.redirectToCheckout(sessionId);
    // } catch (err) {
    // alert(`Error: ${err.message}`);
    // }
  };

  return (
    <button
      type='button'
      className={styles.subscribeButton}
      onClick={handleSubcribe}
    >
      Subscribe Now
    </button>
  );
}
