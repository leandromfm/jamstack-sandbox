<h1 align="center"><strong>JAMStack Sandbox</strong></h1>
<h5 align="center"><strong>Beware, this is a living document and it will be continuously updated!!! 🚀</strong></h5>

<br/>
<blockquote>
  <br/>
  <h2><strong>"Documentation is a love letter that you write to your future self." - Damian Conway</strong></h2>
  <br/>
</blockquote>
<br/>

<!-- <h2>🎯 <strong>Table of Contents</strong></h2>
<br/> -->

<h2>🎯 <strong>Objectives</strong></h2>
<p>
  This repository is an attempt to explore in depth the <strong>JAMStack</strong> by developing from scratch a blog application.
  The main purpose is the experimentation of new tchnologies and their interaction using the best software development practices.  
  <br/><br/>
  
Our Philosophy:

  <ul style="list-style:square">
    <li>S.O.L.I.D Principles</li>    
    <li>Microservices</li>
    <li>Serverless</li>
    <li>Domain Driven Design</li>
    <li>Component Driven UI</li>    
  </ul>
  <br/>

Technologies:

  <ul style="list-style:square">
    <li>Next.js | React.js with Typescript</li>        
    <li>FaunaDB | DynamoDB</li>
    <li>Prismic | Strapi</li>
    <li>Stripe</li>
    <li>AWS</li>    
  </ul>
</p>
<br/>

<h2>⚓️ <strong>Navigating Throughout The Documentation</strong></h2>
<p>The repository contains files with the <strong>.md</strong> extension. Those files contain an overview of its content, which can be any subject from a specific technology to an abstract concept</p>
<br/>

<h2>💻 <strong>Next.js</strong></h2>
<p><img src="./assets/Nextjs_SPA.png"></p>
<p><img src="./assets/Nextjs_SSR.png"></p>
<p><img src="./assets/Nextjs_SSG.png"></p>
<br/>

<!-- <h2>🔍 <strong>Requirements</strong></h2>
<p>👉 Backend specifications: please check the server folder</p>
<p>👉 Frontend specifications: will be accessible when the client folder is ac\</p>
<br/> -->

<h2>📝 <strong>References</strong></h2>
<ol>  
  <li>
    <a href="https://nextjs.org/">
        Next.js
    </a>
  </li>
  <li>
    <a href="https://www.typescriptlang.org/">
        Typescript
    </a>
  </li>    
  <li>
    <a href="https://fauna.com/">
        FaunaDB
    </a>
  </li>    
  <li>
    <a href="https://aws.amazon.com/dynamodb/">
        DynamoDB
    </a>
  </li>    
  <li>
    <a href="https://prismic.io/">
        Prismic
    </a>
  </li>    
  <li>
    <a href="https://strapi.io/">
        Strapi
    </a>
  </li>    
</ol>
<br/>

<h2>🔐 <strong>License</strong></h2>
<p>Copyright © 2021 - This project is <a href="./LICENSE">MIT</a> licensed.</p>
